#!/bin/bash
#SBATCH -J test_cr
#SBATCH -q regular
#SBATCH -N 1 
#SBATCH -C haswell
#SBATCH -t 48:00:00
#SBATCH -o %x-%j.out
#SBATCH -e %x-%j.err
#SBATCH --time-min=6:00:00

#for c/r with dmtcp
module load dmtcp nersc_cr

#checkpointing once every hour
start_coordinator -i 3600

#restarting from dmtcp checkpoint files
./dmtcp_restart_script.sh 


