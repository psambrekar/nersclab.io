# E4S Spack Develop Stack

The E4S Spack Develop Stack is an E4S Stack built using spack [develop](https://github.com/spack/spack/tree/develop)
branch. The entire stack is built from source on a daily basis and stack will change at any given time. All specs will
be built with unconstrained versions in order to get latest version of each package provided by spack. 

## E4S Spack Develop for Perlmutter

We provide a user facing module called `e4s/spack-develop`, you can load this module which will show the following message.

```shell
evlis@perlmutter> ml e4s/spack-develop
=========================================================
Welcome to E4S stack built with spack develop
=========================================================

This stack is rebuilt regularly with tip of spack 'develop' branch which means packages will change over time. You can access the log files via $SPACK_INSTALL_LOG,
$SPACK_CONCRETIZE_LOG, $SPACK_GITLAB_LOG. You can view the spack configuration (spack.yaml, spack.lock) via environment $SPACK_YAML and $SPACK_LOCK.

For more information regarding this stack see https://software.nersc.gov/NERSC/spack-infrastructure.
```

This module will source a spack instance and you can use the **spack** command to interact with the software stack. Note
that we do not generate modulefiles so only way to load software is via `spack load`. We have set a few environment
variables to help you understand how stack was built

 - `SPACK_INSTALL_LOG`  captures output of `spack install` 
 - `$SPACK_CONCRETIZE_LOG`  captures output of `spack concretize` 
 - `SPACK_GITLAB_LOG` captures all Gitlab environment variables (`CI_*`) used during the build
 - `SPACK_YAML` and `SPACK_LOCK` are reference to `spack.yaml` and `spack.lock` file. 

```shell
evlis@perlmutter>  ls $SPACK_INSTALL_LOG $SPACK_CONCRETIZE_LOG $SPACK_GITLAB_LOG $SPACK_YAML $SPACK_LOCK
/global/cfs/cdirs/m3503/spackstacks/perlmutter/spack-develop/spack/concretize.log
/global/cfs/cdirs/m3503/spackstacks/perlmutter/spack-develop/spack/gitlab.log
/global/cfs/cdirs/m3503/spackstacks/perlmutter/spack-develop/spack/install.log
/global/cfs/cdirs/m3503/spackstacks/perlmutter/spack-develop/spack/var/spack/environments/e4s/spack.lock
/global/cfs/cdirs/m3503/spackstacks/perlmutter/spack-develop/spack/var/spack/environments/e4s/spack.yaml
```

Upon loading this module, you will be in `e4s` environment as shown below

```shell
evlis@perlmutter>  spack env st
==> In environment e4s
```

!!! note

    Please note output of `spack find` will change over time, you may experience period of inactivity or
    an empty stack if CI pipeline has failed for external reasons. 
    
As of Jan 28th 2022, we have OpenJDK **openjdk@11.0.12_7** you can load this via `spack load` and it will update your
user environment such as $PATH, $LD_LIBRARY_PATH, etc... We can see below that we are now using `java` from the
spack build of `openjdk@11.0.12_7`. 

```shell
evlis@perlmutter>  date
Fri Jan 28 19:21:27 PST 2022
evlis@perlmutter>  spack find --format "{name}@{version}" openjdk
openjdk@11.0.12_7
evlis@perlmutter> spack load openjdk@11.0.12_7

evlis@perlmutter>  which java
/global/cfs/cdirs/m3503/spackstacks/perlmutter/spack-develop/spack/opt/spack/cray-sles15-zen2/gcc-9.3.0/openjdk-11.0.12_7-5ubf63xvp3ff54kremnd7z4fxi5dse72/bin/java
evlis@perlmutter>  java --version
openjdk 11.0.12 2021-07-20
OpenJDK Runtime Environment Temurin-11.0.12+7 (build 11.0.12+7)
OpenJDK 64-Bit Server VM Temurin-11.0.12+7 (build 11.0.12+7, mixed mode)
```

## E4S Spack Develop for Cori

Similar to Perlmutter, we have an E4S stack built for Cori with a modulefile named `e4s/spack-develop`. You can load this
module and it will expose a spack instance as shown below

```shell
elvis@perlmutter> module load e4s/spack-develop
=========================================================
Welcome to E4S stack built with spack develop
=========================================================
This stack is rebuilt regularly with tip of spack 'develop' branch which means packages will change over time. 

For more information regarding this stack see https://software.nersc.gov/NERSC/spack-infrastructure.
elvis@perlmutter> which spack
/global/cfs/cdirs/m3503/spackstacks/cori/spack-develop/spack/bin/spack
``` 

This modulefile will also set environment variables such as `$SPACK_INSTALL_LOG`, `$SPACK_CONCRETIZE_LOG`, `$SPACK_GITLAB_LOG`,
`$SPACK_YAML` and `$SPACK_LOCK` as shown below

```shell
elvis@perlmutter> ls $SPACK_INSTALL_LOG $SPACK_CONCRETIZE_LOG $SPACK_GITLAB_LOG $SPACK_YAML $SPACK_LOCK
/global/cfs/cdirs/m3503/spackstacks/cori/spack-develop/spack/concretize.log
/global/cfs/cdirs/m3503/spackstacks/cori/spack-develop/spack/gitlab.log
/global/cfs/cdirs/m3503/spackstacks/cori/spack-develop/spack/install.log
/global/cfs/cdirs/m3503/spackstacks/cori/spack-develop/spack/var/spack/environments/e4s/spack.lock
/global/cfs/cdirs/m3503/spackstacks/cori/spack-develop/spack/var/spack/environments/e4s/spack.yaml
```

Similar to Perlmutter, there are no modulefiles generated for this stack and only way to interface with software
stack is using `spack` command line interface.
