# Spin Concepts and Terminology (Rancher 2)

* **Container image**: Blueprint for how a container is
created and started. Similar to a tarball with some other
important metadata added.

* **Container**: Running instance of an image with a
private process and storage space. Similar to a regular
process - especially a jailed one - and can have child
processes. Containers are _ephemeral_; when the
process exits, the container no longer exists.

* **Image Registry**: Versioned repository for container
images. Organized into _namespaces_ (like directories).
Labels are typically applied to images for version control.
For example,

    ```registry.nersc.gov/myproject/myimage:mylabel```

* **Pod**: One or more very-closely-coupled containers.
Pods allow for scaling; for example, a web front-end that
is heavily used might be deployed as a _pod with a scale of
three_, meaning it is configured to run three identical
containers based on the same image, and load distributed
across them.

* **Workload**: Set of parameters and rules that define
how to create and run a particular pod. Includes the image,
scale, settings such as environment variables, storage
volumes, etc.

* **Deploy**: Create a workload.

* **Ingress**: Proxy that allows a workload to be accessible
on the network using a DNS name. The ingress in Spin uses
the nginx web server software internally.

* **Namespace**: Group of workloads. Typically used to
group all of the workloads that make up a particular system,
but can also be used in other ways; for example, to group
all of the workloads that belong to a particular user.

* **Project**: Group of workloads, namespaces, ingresses, etc.
In Spin, these correspond to NERSC projects. Used for access
control; all objects in a project are accessible only to
members of the project.

* **Kubernetes**: Container scheduling system. Responsible
for running all of the above on cluster nodes.

* **Rancher**: Orchestration system for Kubernetes clusters.
Responsible for managing the Kubernetes configuration
and installation. Provides an overarching web user interface,
CLI, and internal API; provides authentication and access
control to clusters and projects.
